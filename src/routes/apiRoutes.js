const router = require("express").Router();
const alunoController = require("../controller/alunoController");
const JSONPlaceholdercontroller = require("../controller/JSONPlaceholdercontroller");

router.post("/cadastro/", alunoController.postAluno);
router.get("/aluno/", alunoController.getAluno);
router.put("/editar/", alunoController.updateAluno);
router.delete("/delete/:id", alunoController.deleteAluno);
router.get("/external/",JSONPlaceholdercontroller.getUsers);
router.get("/getUsersWebsiteIO/",JSONPlaceholdercontroller.getUsersWebsiteIO);
router.get("/getUsersWebsiteCOM/",JSONPlaceholdercontroller.getUsersWebsiteCOM);
router.get("/getUsersWebsiteNET/",JSONPlaceholdercontroller.getUsersWebsiteNET);
router.get("/external/filter", JSONPlaceholdercontroller.getCountDomain);



module.exports = router;
